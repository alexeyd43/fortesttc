
namespace MailList.Repositories.Entities
{
  public class DbMailList
  {
    public long Id { get; set; }
    public string Name { get; set; }
    public long? FilterId { get; set; }
    public long? UniSenderId { get; set; }
    public bool IsDeleted { get; set; }
    public bool IsDeletedFromUniSender { get; set; }
  }
}
