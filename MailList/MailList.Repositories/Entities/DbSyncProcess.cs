using System;

namespace MailList.Repositories.Entities
{
  public class DbSyncProcess
  {
    public long Id { get; set; }
    public DateTime StartedAt { get; set; }
    public DateTime? EndedAt { get; set; }
    public short? CurrentProgress { get; set; }
    public int? TotalContactCount { get; set; }
  }
}
