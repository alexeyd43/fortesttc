using System;

namespace MailList.Repositories.Entities
{
  public class DbSyncSchedule
  {
    public TimeSpan SyncAt { get; set; }
    public string Days { get; set; }
    public bool ByWeekdays { get; set; }
    public bool Enabled { get; set; }
  }
}
