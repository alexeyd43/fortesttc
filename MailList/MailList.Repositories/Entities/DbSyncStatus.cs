
namespace MailList.Repositories.Entities
{
  public class DbSyncStatus
  {
    public long Id { get; set; }
    public long SyncProcessId { get; set; }
    public long MailGroupId { get; set; }
    public long? FilterId { get; set; }
    public int? OperationType { get; set; }
    public int? TotalContactCount { get; set; }
    public int? SyncedContactCount { get; set; }
    public int? SyncResultStatus { get; set; }
    public string FailMessage { get; set; }
  }
}
