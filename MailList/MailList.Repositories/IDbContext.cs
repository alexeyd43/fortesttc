using MailList.Repositories.Interfaces;
using System.Data;

namespace MailList.Repositories
{
  public interface IDbContext
  {
    IDbConnection Connection { get; set; }
    IDbMailGroups MailGroups { get; }
    IDbSyncProcesses SyncProcesses { get; }
    IDbSyncStatuses SyncStatuses { get; }
    IDbSyncSchedule SyncSchedule { get; }
  }
}
