using Insight.Database;
using MailList.Repositories.Interfaces;
using System;
using System.Data;
using System.Data.Common;

namespace MailList.Repositories
{
  public class DbContext : IDbContext
  {
    public IDbConnection Connection { get; set; }

    public IDbMailGroups MailGroups => CreateRepository<IDbMailGroups>();
    public IDbSyncProcesses SyncProcesses => CreateRepository<IDbSyncProcesses>();
    public IDbSyncStatuses SyncStatuses => CreateRepository<IDbSyncStatuses>();
    public IDbSyncSchedule SyncSchedule => CreateRepository<IDbSyncSchedule>();

    public DbContext(DbConnectionSettings connectionStringSettings)
    {
      SqlInsightDbProvider.RegisterProvider();

      var providerFactory = DbProviderFactories.GetFactory(DbConnectionSettings.ProviderName);
      Connection = providerFactory.CreateConnection() ?? throw new Exception("Connection object is null");
      Connection.ConnectionString = connectionStringSettings.ConnectionString;
    }

    public TRepository CreateRepository<TRepository>() where TRepository : class
    {
      return Connection.As<TRepository>();
    }
  }
}
