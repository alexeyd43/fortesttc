using Insight.Database;
using MailList.Repositories.Entities;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace MailList.Repositories.Interfaces
{
  [Sql(Schema = "dbo")]
  public interface IDbSyncStatuses : IDbConnection, IDbTransaction
  {
    Task InsertSyncStatusAsync(DbSyncStatus syncStatus);
    Task UpdateSyncStatusAsync(DbSyncStatus syncStatus);
    Task<DbSyncStatus> GetSyncStatusAsync(long id);
    Task<IList<DbSyncStatus>> GetSyncStatusListByProcessIdAsync(long processId);
    Task<IList<DbSyncStatus>> GetSyncStatusListAsync();
  }
}
