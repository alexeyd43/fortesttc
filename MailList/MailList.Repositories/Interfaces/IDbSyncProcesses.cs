using Insight.Database;
using MailList.Repositories.Entities;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace MailList.Repositories.Interfaces
{
  [Sql(Schema = "dbo")]
  public interface IDbSyncProcesses : IDbConnection, IDbTransaction
  {
    Task InsertSyncProcessAsync(DbSyncProcess syncProcess);
    Task UpdateSyncProcessAsync(DbSyncProcess syncProcess);
    Task<DbSyncProcess> GetSyncProcessAsync(long id);
    Task<DbSyncProcess> GetLastSyncProcessAsync();
    Task<IList<DbSyncProcess>> GetSyncProcessListAsync();
  }
}
