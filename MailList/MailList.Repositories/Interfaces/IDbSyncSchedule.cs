using Insight.Database;
using MailList.Repositories.Entities;
using System.Data;
using System.Threading.Tasks;

namespace MailList.Repositories.Interfaces
{
  [Sql(Schema = "dbo")]
  public interface IDbSyncSchedule : IDbConnection, IDbTransaction
  {
    Task<DbSyncSchedule> GetSyncScheduleAsync();
    Task UpsertSyncScheduleAsync(DbSyncSchedule syncSchedule);
  }
}
