using Insight.Database;
using MailList.Repositories.Entities;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace MailList.Repositories.Interfaces
{
  [Sql(Schema = "dbo")]
  public interface IDbMailGroups : IDbConnection, IDbTransaction
  {
    Task InsertMailGroupAsync(DbMailList mailList);
    Task UpdateMailGroupAsync(DbMailList mailList);
    Task<DbMailList> GetMailGroupAsync(long id);
    Task<IList<DbMailList>> GetMailGroupListAsync();
    Task DeleteMailGroupAsync(long id);
  }
}
