﻿using Common.Messages.JobScheduler;
using System;

namespace XCritical.Common.Messages.JobScheduler
{
  public class JobSchedulerEvent
  {
    public JobData JobData { get; set; }
    public DateTimeOffset? ScheduledFireTimeUtc { get; set; }
  }
}
