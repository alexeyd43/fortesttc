﻿using System;

namespace Common.Messages.JobScheduler
{
  public class JobData
  {
    public string Id { get; set; }
    public int JobType { get; set; }
    public string ExternalKey { get; set; }
    public string Schedule { get; set; }
    public DateTime? FireTime { get; set; }
  }
}
