print ''
print N'Begin Post-deployment.'

declare @curver nvarchar(8)
select @curver = cast([DbVersion] as nvarchar(8)) from dbo.DbVersion

print N'Current version is: ' + @curver

if @curver is null
begin
  insert into dbo.DbVersion ([Id], [Type], [DbVersion], [Created]) values (N'$(DbVer)', N'$(DbType)', '20191201', getutcdate())
  set @curver = '20191201'
end

/*

if @curver < 'YYYYMMDD'
begin
  print N'--Executing script: PostDeployment_YYYYMMDD'
  :r .\PostDeployment_YYYYMMDD.sql
  set @curver = 'YYYYMMDD'
end

*/

print N'Setting up version: ' + @curver
update dbo.DbVersion set Id = N'$(DbVer)', [DbVersion] = @curver, Created = getutcdate()

print N'End Post-deployment.'
print ''