if exists(select 1 from master.dbo.sysdatabases where name = N'$(DatabaseName)'
  and exists(select 1 from sys.objects where Name = 'DbVersion' and type = 'U'))
begin

  print ''
  print N'Begin Pre-deployment.'

  declare @curver nvarchar(8)
  select @curver = cast([DbVersion] as nvarchar(8)) from dbo.DbVersion
  
  print N'Current version is: ' + @curver

  /*
  
  if @curver < 'YYYYMMDD'
  begin
    print N'--Executing script: PreDeployment_YYYYMMDD'
    :r .\PreDeployment_YYYYMMDD.sql
    set @curver = 'YYYYMMDD'
  end

  */
  
  print N'End Pre-deployment.'
  print ''

end