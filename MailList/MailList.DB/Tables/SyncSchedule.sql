create table [dbo].[SyncSchedule]
(
  [SyncAt] time(0) null,
  [Days] varchar(255) null,
  [ByWeekdays] bit not null,
  [Enabled] bit not null
)
