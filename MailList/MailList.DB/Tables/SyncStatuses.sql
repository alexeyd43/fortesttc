create table [dbo].[SyncStatuses]
(
  [Id] bigint identity(1,1) primary key,
  [SyncProcessId] bigint not null,
  [MailGroupId] bigint not null,
  [FilterId] bigint null,
  [OperationType] smallint null,
  [TotalContactCount] int null,
  [SyncedContactCount] int null,
  [SyncResultStatus] smallint null,
  [FailMessage] nvarchar(max) null,

  constraint FK_SyncStatuses_SyncProcess foreign key (SyncProcessId) references dbo.SyncProcesses(Id),
  constraint FK_SyncStatuses_MailGroup foreign key (MailGroupId) references dbo.[MailLists](Id)
)
