create table dbo.DbVersion
(
  [Id] varchar(64)  not null, --$(DbVer)
  [Created] datetime not null constraint DF_DbVersion_Created default getutcdate(), 
  [Type] nvarchar(32) not null,
  [DbVersion] nvarchar(8)  not null
)
