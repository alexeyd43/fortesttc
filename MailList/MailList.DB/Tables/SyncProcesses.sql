create table [dbo].[SyncProcesses]
(
  [Id] bigint identity(1,1) primary key,
  [StartedAt] datetime2 not null default getutcdate(),
  [EndedAt] datetime2 null,
  [CurrentProgress] smallint null,
  [TotalContactCount] int null
)
