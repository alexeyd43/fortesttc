create table [dbo].[MailLists]
(
  [Id] bigint identity(1,1) primary key,
  [Name] nvarchar(255) unique not null,
  [FilterId] bigint null,
  [UniSenderId] bigint null,
  [IsDeleted] bit null,
  [IsDeletedFromUniSender] bit not null default 0
)
