create procedure dbo.GetSyncSchedule
as
begin
  select SyncAt, [Days], ByWeekdays from dbo.SyncSchedule
end