create procedure dbo.UpsertSyncSchedule
  @syncAt time(0),
  @days varchar(255),
  @byWeekdays bit
as
begin
  delete from dbo.SyncSchedule;

  insert into dbo.SyncSchedule(SyncAt, Days, ByWeekdays)
  output
  inserted.SyncAt,
  inserted.Enabled,
  inserted.Days,
  inserted.ByWeekdays
  values (@syncAt, @days, @byWeekdays)
end