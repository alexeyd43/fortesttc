create procedure dbo.UpdateSyncStatus
  @id bigint,
  @syncProcessId bigint,
  @mailGroupId bigint,
  @filterId bigint,
  @operationType smallint,
  @totalContactCount int,
  @syncedContactCount int,
  @syncStatus smallint,
  @failMessage nvarchar(max)
as
begin
  update dbo.[SyncStatuses] set
    SyncProcessId = @syncProcessId,
    MailGroupId = @mailGroupId,
    FilterId = @filterId,
    OperationType = @operationType,
    TotalContactCount = @totalContactCount,
    SyncedContactCount = @syncedContactCount,
    [SyncResultStatus] = @syncStatus,
    FailMessage = @failMessage
  where Id = @id

  exec dbo.GetSyncStatus @id
end