create procedure dbo.GetSyncStatusList
as
begin
  select Id, SyncProcessId, MailGroupId, FilterId, OperationType, TotalContactCount, SyncedContactCount, [SyncResultStatus], FailMessage from dbo.[SyncStatuses]
end