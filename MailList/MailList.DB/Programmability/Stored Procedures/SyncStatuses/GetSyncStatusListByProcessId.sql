create procedure dbo.GetSyncStatusListByProcessId
  @processId bigint
as
begin
  select Id, SyncProcessId, MailGroupId, FilterId, OperationType, TotalContactCount, SyncedContactCount, [SyncResultStatus], FailMessage from dbo.[SyncStatuses]
    where SyncProcessId = @processId
end