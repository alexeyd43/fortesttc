create procedure dbo.GetSyncStatus
  @id bigint
as
begin
  select Id, SyncProcessId, MailGroupId, FilterId, OperationType, TotalContactCount, SyncedContactCount, [SyncResultStatus], FailMessage from dbo.[SyncStatuses]
    where Id = @id
end