create procedure dbo.InsertSyncStatus
  @syncProcessId bigint,
  @mailGroupId bigint,
  @filterId bigint,
  @operationType smallint,
  @totalContactCount int,
  @syncedContactCount int,
  @syncStatus smallint,
  @failMessage nvarchar(max) null
as
begin
  insert into dbo.[SyncStatuses](SyncProcessId, MailGroupId, FilterId, OperationType, TotalContactCount, SyncedContactCount, SyncResultStatus, FailMessage)
  output
  inserted.Id,
  inserted.SyncProcessId,
  inserted.MailGroupId,
  inserted.FilterId,
  inserted.OperationType,
  inserted.TotalContactCount,
  inserted.SyncedContactCount,
  inserted.SyncResultStatus,
  inserted.FailMessage
    values (@syncProcessId, @mailGroupId, @filterId, @operationType, @totalContactCount, @syncedContactCount, @syncStatus, @failMessage)
end