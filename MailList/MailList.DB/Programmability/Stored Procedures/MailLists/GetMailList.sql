create procedure dbo.GetMailList
  @id bigint
as
begin
  select Id, [Name], FilterId, UniSenderId, IsDeleted, IsDeletedFromUniSender from dbo.[MailLists]
  where Id = @id
end