create procedure dbo.DeleteMailList
  @id bigint
as
begin
  delete from dbo.[MailLists]
  where Id = @id
end
