create procedure dbo.InsertMailList
  @name nvarchar(255),
  @filterId bigint,
  @uniSenderId bigint,
  @isDeleted bit,
  @isDeletedFromUniSender bit
as
begin
  insert into dbo.[MailLists]([Name], FilterId, UniSenderId, IsDeleted, IsDeletedFromUniSender)
  output
  inserted.Id,
  inserted.Name,
  inserted.FilterId,
  inserted.UniSenderId,
  inserted.IsDeleted,
  inserted.IsDeletedFromUniSender
    values (@name, @filterId, @uniSenderId, @isDeleted, @isDeletedFromUniSender)
end