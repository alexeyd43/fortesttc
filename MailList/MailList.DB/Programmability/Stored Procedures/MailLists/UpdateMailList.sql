create procedure dbo.UpdateMailList
  @id bigint,
  @name nvarchar(255),
  @filterId bigint,
  @uniSenderId bigint,
  @isDeleted bit,
  @isDeletedFromUniSender bit
as
begin
  update dbo.[MailLists] set
    [Name] = @name,
    FilterId = @filterId,
    UniSenderId = @uniSenderId,
    IsDeleted = @isDeleted,
    IsDeletedFromUniSender = @isDeletedFromUniSender
  where Id = @id

  exec dbo.GetMailList @id
end