create procedure dbo.GetMailLists
as
begin
  select Id, [Name], FilterId, UniSenderId, IsDeleted, IsDeletedFromUniSender from dbo.[MailLists]
end