create procedure dbo.GetSyncProcess
  @id bigint
as
begin
  select Id, StartedAt, EndedAt, CurrentProgress, TotalContactCount from dbo.SyncProcesses
  where Id = @id
end
