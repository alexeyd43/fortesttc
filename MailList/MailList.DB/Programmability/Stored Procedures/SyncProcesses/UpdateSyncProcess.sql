create procedure dbo.UpdateSyncProcess
  @id bigint,
  @startedAt datetime2,
  @endedAt datetime2,
  @currentProgress smallint,
  @totalContactCount int
as
begin
  update dbo.SyncProcesses set
    StartedAt = @startedAt,
    EndedAt = @endedAt,
    CurrentProgress = @currentProgress,
    TotalContactCount = @totalContactCount
  where Id = @id

  exec dbo.GetSyncProcess @id
end
