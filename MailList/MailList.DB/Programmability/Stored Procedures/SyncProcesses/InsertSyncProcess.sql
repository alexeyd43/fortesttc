create procedure dbo.InsertSyncProcess
  @startedAt datetime2,
  @endedAt datetime2,
  @currentProgress smallint,
  @totalContactCount int
as
begin
  insert into dbo.SyncProcesses(StartedAt, EndedAt, CurrentProgress, TotalContactCount)
  output
  inserted.Id,
  inserted.StartedAt,
  inserted.EndedAt,
  inserted.CurrentProgress,
  inserted.TotalContactCount
    values (@startedAt, @endedAt, @currentProgress, @totalContactCount)
end