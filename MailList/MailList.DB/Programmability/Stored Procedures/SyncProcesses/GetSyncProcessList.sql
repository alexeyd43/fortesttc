create procedure dbo.GetSyncProcessList
as
begin
  select Id, StartedAt, EndedAt, CurrentProgress, TotalContactCount from dbo.SyncProcesses
end
