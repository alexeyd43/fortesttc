create procedure dbo.GetLastSyncProcess
as
begin
  select top 1 Id, StartedAt, EndedAt, CurrentProgress, TotalContactCount from dbo.SyncProcesses
  order by StartedAt desc
end
