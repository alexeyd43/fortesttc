using System;

namespace XCritical.Crm.MailList.Core.Exceptions
{
  public class InvalidArgumentException : Exception
  {
    public InvalidArgumentException()
    {
    }

    public InvalidArgumentException(string message) : base(message)
    {
    }
  }
}