using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace XCritical.Crm.MailList.API.V1.Controllers
{
  [ApiVersion(Startup.Version1_0)]
  [Route("{version:apiVersion}/mail-groups")]
  [ApiController]
  public class MailListController : Controller
  {
    private ILogger<MailListController> _logger;

    public MailListController(ILogger<MailListController> logger)
    {
      _logger = logger;
    }
  }
}