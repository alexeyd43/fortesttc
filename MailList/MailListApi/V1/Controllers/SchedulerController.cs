﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using XCritical.Crm.MailList.Logic.Services.SyncScheduleService;

namespace XCritical.Crm.MailList.API.V1.Controllers
{
  [ApiVersion(Startup.Version1_0)]
  [Route("{version:apiVersion}/sceduler")]
  [ApiController]
  public class SchedulerController : Controller
  {
    private ILogger<SchedulerController> _logger;
    private ISyncScheduleService _scheduleService;

    public SchedulerController(ILogger<SchedulerController> logger, ISyncScheduleService scheduleService)
    {
      _logger = logger;
      _scheduleService = scheduleService;
    }

    [HttpPost]
    public async Task<IActionResult> CreateOrUpdateSchedule(SyncSchedule schedule)
    {
      try
      {
        await _scheduleService.UpsertSyncScheduleAsync(schedule);
        return Ok();
      }
      catch (Exception e)
      {
        _logger.LogError(e.Message, e);
        throw;
      }
    }

    [HttpGet]
    public async Task<IActionResult> GetSchedule()
    {
      try
      {
        var schedule = await _scheduleService.GetSyncScheduleAsync();
        return Ok(schedule);
      }
      catch (Exception e)
      {
        _logger.LogError(e.Message, e);
        throw;
      }
    }
  }
}
