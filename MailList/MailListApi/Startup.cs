using Autofac;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Swashbuckle.AspNetCore.Swagger;
using XCritical.Crm.MailList.API.Infrastructure;

namespace XCritical.Crm.MailList.API
{
  public class Startup
  {
    public const string Version1_0 = "1.0";
    private readonly IConfiguration _configuration;

    public Startup(IConfiguration configuration)
    {
      _configuration = configuration;
    }


    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
      services.AddMvcCore();

      services.AddMvc().AddJsonOptions(options =>
      {
        options.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
        options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
        options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
        options.SerializerSettings.TypeNameHandling = TypeNameHandling.None;
      });

      services.AddHttpContextAccessor();
      services.AddApiVersioning();
      services.AddSwaggerGen(c =>
      {
        c.SwaggerDoc("v1", new Info { Title = "MailList API", Version = "v1" });
      });

      services.AddHostedService<BusDriverHostedService>();
    }

    public void ConfigureContainer(ContainerBuilder builder)
    {
      builder.RegisterModule(new AutofacModule(_configuration));
    }

    public void Configure(IApplicationBuilder app, IHostingEnvironment env)
    {
      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
      }

      bool.TryParse(_configuration["EnableSwagger"], out var swaggerEnabled);
      if (swaggerEnabled)
      {
        app.UseSwagger();
        app.UseSwaggerUI(c =>
        {
          c.SwaggerEndpoint("/swagger/v1/swagger.json", "MailList API V1");
        });
      }
      
      app.UseMvc();
    }
  }
}
