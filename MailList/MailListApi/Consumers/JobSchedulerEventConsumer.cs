﻿using JobScheduler.Client.Models;
using MassTransit;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using XCritical.Common.Messages.JobScheduler;
using XCritical.Crm.MailList.Logic.Services.SyncService;

namespace XCritical.Crm.MailList.API.Consumers
{
  public class JobSchedulerEventConsumer : IConsumer<JobSchedulerEvent>
  {
    private readonly ILogger _logger;
    private readonly IMailListSyncService _syncService;
    public JobSchedulerEventConsumer(ILogger<JobSchedulerEventConsumer> logger, IMailListSyncService syncService)
    {
      _logger = logger;
      _syncService = syncService;
    }

    public async Task Consume(ConsumeContext<JobSchedulerEvent> context)
    {
      try
      {
        var jobSchedulerEvent = context.Message;

        if ((JobType)jobSchedulerEvent.JobData.JobType == JobType.MailGroupsSynchronizationTrigger)
        {
          await _syncService.Synchronize();
        }
      }
      catch (Exception e)
      {
        _logger.LogError("Unknown error", e);
        throw;
      }
    }
  }
}
