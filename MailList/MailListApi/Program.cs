using System.Diagnostics;
using System.IO;
using System.Linq;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.WindowsServices;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace XCritical.Crm.MailList.API
{
  public class Program
  {
    public static void Main(string[] args)
    {
      var isService = !(Debugger.IsAttached || args.Contains("--console"));

      var builder = CreateWebHostBuilder(
        args.Where(arg => arg != "--console").ToArray());

      if (isService)
      {
        var pathToExe = Process.GetCurrentProcess().MainModule?.FileName;
        var pathToContentRoot = Path.GetDirectoryName(pathToExe);
        builder.UseContentRoot(pathToContentRoot);
      }
      var host = builder.Build();
      if (isService)
      {
        host.RunAsService();
      }
      else
      {
        host.Run();
      }
    }

    public static IWebHostBuilder CreateWebHostBuilder(string[] args)
    {
      var config = new ConfigurationBuilder()
      .AddJsonFile("appsettings.json")
      .Build();

      return WebHost.CreateDefaultBuilder(args)
      .UseConfiguration(config)
      .UseStartup<Startup>()
      .ConfigureServices(services => services.AddAutofac())
      .UseSerilog((context, с) => с.ReadFrom.Configuration(context.Configuration));
    }
  }
}
