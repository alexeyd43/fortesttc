using Autofac;
using GreenPipes;
using JobScheduler.Client;
using MassTransit;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using XCritical.Crm.MailList.API.Consumers;
using XCritical.Crm.MailList.Logic.Services.SyncScheduleService;
using XCritical.Crm.MailList.Logic.Services.SyncService;

namespace XCritical.Crm.MailList.API
{
  public class AutofacModule : Module
  {
    private readonly IConfiguration _configuration;

    public AutofacModule(IConfiguration configuration)
    {
      _configuration = configuration;
    }

    protected override void Load(ContainerBuilder builder)
    {
      builder.RegisterConsumers(System.Reflection.Assembly.GetExecutingAssembly()).SingleInstance();
      builder.Register(RegisterBusControl).As<IBusControl>().SingleInstance();
      builder.RegisterType<SyncScheduleService>().As<ISyncScheduleService>().InstancePerLifetimeScope();
      builder.RegisterType<MailListSyncService>().As<IMailListSyncService>();
      builder.Register(x => new JobSchedulerClient(_configuration["JobScheduler:ClientBaseUrl"])).As<IJobSchedulerClient>();
      builder.Register(s => new SchedulerConfiguration
      {
        SyncScheduleId = Guid.Parse(_configuration["JobScheduler:SyncScheduleId"])
      }).As<SchedulerConfiguration>().SingleInstance();
    }

    private IBusControl RegisterBusControl(IComponentContext context)
    {
      var busControl = Bus.Factory.CreateUsingRabbitMq(bfc =>
      {
        var host = bfc.Host(new Uri(_configuration["Bus:Host"]), c =>
        {
          c.Username(_configuration["Bus:Username"]);
          c.Password(_configuration["Bus:Password"]);
        });

        bfc.OverrideDefaultBusEndpointQueueName(_configuration["Bus:TemporaryQueue"]);
        bfc.AutoDelete = false;

        bfc.ReceiveEndpoint(host, _configuration["Bus:Queue"], rec =>
        {
          rec.Consumer<JobSchedulerEventConsumer>(context.Resolve<ILifetimeScope>());
          rec.UseRetry(rc => rc.Immediate(5));
        });

        bfc.UseExtensionsLogging(context.Resolve<ILoggerFactory>());
      });

      return busControl;
    }
  }
}
