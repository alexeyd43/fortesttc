﻿
namespace JobScheduler.Client.Models
{
  public enum JobType
  {
    NewLeadsForSalesMessageTrigger = 1,
    CalendarEventMessageTrigger = 2,
    CriticalEquityAndOpenPnlTrigger = 3,
    ClearUserStatisticTrigger = 4,
    SearchAndMarkLeadDuplicatesTrigger = 5,
    CheckDocumentsTrigger = 6,
    MailGroupsSynchronizationTrigger = 7
  }
}
