﻿using System;

namespace JobScheduler.Client.Models
{
  public class Job
  {
    public Guid Id { get; set; }
    public JobType JobType { get; set; }
    public string ExternalKey { get; set; }
    public string Schedule { get; set; }
    public DateTime? FireTime { get; set; }

    public Job()
    {
      Id = Guid.NewGuid();
    }
  }
}
