﻿using JobScheduler.Client.Models;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;

namespace JobScheduler.Client
{
  public class JobSchedulerClient : IJobSchedulerClient, IDisposable
  {
    private readonly HttpClient _client;
    public JobSchedulerClient(string baseUrl)
    {
      _client = new HttpClient { BaseAddress = new Uri(baseUrl) };
    }
    public async Task<Guid> CreateJobAsync(Job job)
    {
      if (job == null)
      {
        throw new ArgumentNullException(nameof(job));
      }
      var response = await _client.PostAsync("api/jobs", new ObjectContent(job.GetType(), job, new JsonMediaTypeFormatter(), "application/json"));
      if (!response.IsSuccessStatusCode)
      {
        throw new Exception($"Request to {response.RequestMessage.RequestUri} failed with status code {(int)response.StatusCode}");
      }
      var jobId = await response.Content.ReadAsAsync<Guid>();
      return jobId;
    }

    public async Task<Job> GetJobAsync(Guid id)
    {
      if (id == Guid.Empty)
      {
        throw new ArgumentException("Id cannot be empty guid", nameof(id));
      }
      var response = await _client.GetAsync($"api/jobs/{id}");

      if (!response.IsSuccessStatusCode)
      {
        if (response.StatusCode == HttpStatusCode.NotFound)
        {
          return null;
        }
        throw new Exception($"Request to {response.RequestMessage.Method} {response.RequestMessage.RequestUri} failed with status code {(int)response.StatusCode}");
      }
      var job = await response.Content.ReadAsAsync<Job>();
      return job;
    }

    public async Task UpdateJobAsync(Job job)
    {
      if (job == null)
      {
        throw new ArgumentNullException(nameof(job));
      }
      var response = await _client.PutAsync($"api/jobs/{job.Id}", new ObjectContent(job.GetType(), job, new JsonMediaTypeFormatter(), "application/json"));
      if (!response.IsSuccessStatusCode)
      {
        if (response.StatusCode != HttpStatusCode.NotFound)
        {
          throw new Exception($"Request to {response.RequestMessage.RequestUri} failed with status code {(int)response.StatusCode}");
        }
      }
    }

    public async Task DeleteJobAsync(Guid id)
    {
      if (id == Guid.Empty)
      {
        throw new ArgumentException("Id cannot be empty guid", nameof(id));
      }
      var response = await _client.DeleteAsync($"api/jobs/{id}");

      if (!response.IsSuccessStatusCode)
      {
        if (response.StatusCode != HttpStatusCode.NotFound)
        {
          throw new Exception($"Request to {response.RequestMessage.Method} {response.RequestMessage.RequestUri} failed with status code {(int)response.StatusCode}");
        }
      }
    }

    public void Dispose()
    {
      _client?.Dispose();
    }
  }
}
