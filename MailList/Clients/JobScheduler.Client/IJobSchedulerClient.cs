﻿using JobScheduler.Client.Models;
using System;
using System.Threading.Tasks;

namespace JobScheduler.Client
{
  public interface IJobSchedulerClient
  {
    Task<Guid> CreateJobAsync(Job job);
    Task<Job> GetJobAsync(Guid id);
    Task UpdateJobAsync(Job job);
    Task DeleteJobAsync(Guid id);
  }
}
