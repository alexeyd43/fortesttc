using Newtonsoft.Json;

namespace XCritical.Crm.MailList.UniSender.Client.Models.Responses
{
  public class ContactResult
  {
    [JsonProperty("email")]
    public ContactEmail ContactEmail { get; set; }

    [JsonProperty("fields")]
    public ContactFields ContactFields { get; set; }

    [JsonProperty("lists")]
    public ContactList[] ContactLists { get; set; }
  }
}