using System.Collections.Generic;

namespace XCritical.Crm.MailList.UniSender.Client.Models.Responses
{
  public class ListsResponseMessage : BaseResponseMessage<List<ContactListResult>>
  {
  }
}