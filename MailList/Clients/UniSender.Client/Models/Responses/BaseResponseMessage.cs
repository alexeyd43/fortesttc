namespace XCritical.Crm.MailList.UniSender.Client.Models.Responses
{
  public class BaseResponseMessage<T>
  {
    public string Error { get; set; }

    public string Code { get; set; }

    public T Result { get; set; }
  }
}