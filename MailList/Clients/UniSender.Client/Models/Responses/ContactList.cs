using System;
using Newtonsoft.Json;

namespace XCritical.Crm.MailList.UniSender.Client.Models.Responses
{
  public class ContactList
  {
    public long Id { get; set; }

    public string Status { get; set; }

    [JsonProperty("added_at")]
    public DateTimeOffset AddedAt { get; set; }
  }
}