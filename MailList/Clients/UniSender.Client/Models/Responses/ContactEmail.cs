using System;
using Newtonsoft.Json;

namespace XCritical.Crm.MailList.UniSender.Client.Models.Responses
{
  public class ContactEmail
  {
    public string Email { get; set; }

    [JsonProperty("added_at")]
    public DateTime AddedAt { get; set; }

    public string Status { get; set; }

    public string Availability { get; set; }
  }
}