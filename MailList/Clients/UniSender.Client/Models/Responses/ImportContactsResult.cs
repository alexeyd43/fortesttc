using System.Collections.Generic;
using Newtonsoft.Json;

namespace XCritical.Crm.MailList.UniSender.Client.Models.Responses
{
  public class ImportContactsResult
  {
    public long Total { get; set; }

    public long Inserted { get; set; }

    public long Updated { get; set; }

    public long Deleted { get; set; }

    [JsonProperty("new_emails")]
    public long NewEmails { get; set; }

    public long Invalid { get; set; }

    public List<ImportContactLog> Log { get; set; }
  }
}
