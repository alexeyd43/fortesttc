using Newtonsoft.Json;

namespace XCritical.Crm.MailList.UniSender.Client.Models.Responses
{
  public class CreateListResult
  {
    [JsonProperty("id")]
    public long ListId { get; set; }
  }
}