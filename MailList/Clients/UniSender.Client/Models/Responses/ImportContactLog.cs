namespace XCritical.Crm.MailList.UniSender.Client.Models.Responses
{
  public class ImportContactLog
  {
    public long Index { get; set; }
    
    public string Code { get; set; }

    public string Message { get; set; }
  }
}
