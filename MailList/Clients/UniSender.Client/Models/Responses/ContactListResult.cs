namespace XCritical.Crm.MailList.UniSender.Client.Models.Responses
{
  public class ContactListResult
  {
    public long Id { get; set; }

    public string Title { get; set; }
  }
}