namespace XCritical.Crm.MailList.UniSender.Client.Models.Responses
{
  public class ContactCountResult
  {
    public long ListId { get; set; }

    public SearchParams SearchParams { get; set; }

    public long Count { get; set; }
  } 
}