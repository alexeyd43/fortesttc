namespace XCritical.Crm.MailList.UniSender.Client.Models.Requests
{
  public class Contact
  {
    public string Email { get; set; }
    public string Name { get; set; }
    public long ListId { get; set; }
  }
}