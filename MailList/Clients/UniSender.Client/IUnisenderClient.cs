using System.Collections.Generic;
using System.Threading.Tasks;
using XCritical.Crm.MailList.UniSender.Client.Models.Requests;
using XCritical.Crm.MailList.UniSender.Client.Models.Responses;

namespace XCritical.Crm.MailList.UniSender.Client
{
  public interface IUniSenderClient
  {
    Task<ImportContactsResponse> ImportContactsAsync(List<Contact> contacts);

    Task<ListsResponseMessage> GetAllListsAsync();

    Task<CreateListResponseMessage> CreateListAsync(string listName);

    Task<UpdateListResponseMessage> UpdateListAsync(long listId, string newListName);

    Task<DeleteListResponseMessage> DeleteListAsync(long listId);

    Task<ExcludeResponseMessage> ExcludeAsync(string type, string contact, List<long> listIds);

    Task<ContactResponseMessage> GetContactAsync(string type, string contact);

    Task<IsContactInListsResponse> IsContactInListsAsync(string email, List<long> listIds, string condition);

    Task<ContactCountResponseMessage> GetContactCountByListIdAsync(long listId);
  }
}
