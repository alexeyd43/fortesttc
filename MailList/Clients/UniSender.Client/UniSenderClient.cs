using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using RestSharp;
using XCritical.Crm.MailList.UniSender.Client.Models.Requests;
using XCritical.Crm.MailList.UniSender.Client.Models.Responses;
using XCritical.HttpClient;
using XCritical.HttpClient.Settings;

namespace XCritical.Crm.MailList.UniSender.Client
{
  public class UniSenderClient : IUniSenderClient
  {
    private const string GetContactCountMethod = "getContactCount";
    private const string GetListsMethod = "getLists";
    private const string CreateListMethod = "createList";
    private const string UpdateListMethod = "updateList";
    private const string DeleteListMethod = "deleteList";
    private const string ExcludeMethod = "exclude";
    private const string GetContactMethod = "getContact";
    private const string IsContactInListsMethod = "isContactInLists";
    private const string ImportContactsMethod = "importContacts";

    private readonly IRestApiClient _client;
    private readonly string _key;
    private readonly ILogger<IUniSenderClient> _logger;

    public UniSenderClient(string baseUrl, string key, ILogger<IUniSenderClient> logger)
    {
      _client = new RestApiClient(new RestApiClientSettings
      {
        BaseUrl = new Uri(baseUrl)
      });
      _key = key;
      _logger = logger;
    }

    public UniSenderClient(IRestApiClient client, ILogger<IUniSenderClient> logger)
    {
      _client = client;
      _logger = logger;
    }

    public UniSenderClient(string baseUrl, string key, ILogger<UniSenderClient> logger, int timeOutSeconds)
    {
      _client = new RestApiClient(new RestApiClientSettings
      {
        BaseUrl = new Uri(baseUrl),
        Timeout = timeOutSeconds * 1000
      });
      _key = key;
      _logger = logger;
    }

    public async Task<ContactCountResponseMessage> GetContactCountByListIdAsync(long listId)
    {
      _logger.LogInformation($"{GetContactCountMethod} UniSender method was called for ListId: {listId}");
      var url = $"/{GetContactCountMethod}?format=json&api_key={_key}&list_id={listId}&params[type]=address";
      return await _client.GetAsync<ContactCountResponseMessage>(url);
    }

    public async Task<ListsResponseMessage> GetAllListsAsync()
    {
      _logger.LogInformation($"{GetListsMethod} UniSender method was called");
      var url = $"/{GetListsMethod}?format=json&api_key={_key}";
      return await _client.GetAsync<ListsResponseMessage>(url);
    }

    public async Task<CreateListResponseMessage> CreateListAsync(string listName)
    {
      _logger.LogDebug($"{CreateListMethod} UniSender method was called:List name is {listName}");
      var url = $"/{CreateListMethod}?format=json&api_key={_key}&title={listName}";
      return await _client.GetAsync<CreateListResponseMessage>(url);
    }

    public async Task<UpdateListResponseMessage> UpdateListAsync(long listId, string newListName)
    {
      _logger.LogDebug($"{UpdateListMethod} UniSender method was called for List id {listId} and new name is {newListName}");
      var url = $"/{UpdateListMethod}?format=json&api_key={_key}&list_id={listId}&title={newListName}";
      return await _client.GetAsync<UpdateListResponseMessage>(url);
    }

    public async Task<DeleteListResponseMessage> DeleteListAsync(long listId)
    {
      _logger.LogDebug($"{DeleteListMethod} UniSender method was called for List id {listId}");
      var url = $"/{DeleteListMethod}?format=json&api_key={_key}&list_id={listId}";
      return await _client.GetAsync<DeleteListResponseMessage>(url);
    }

    public async Task<ExcludeResponseMessage> ExcludeAsync(string type, string contact, List<long> listIds)
    {
      string separatedListIds = string.Join(",", listIds);
      _logger.LogDebug($"{ExcludeMethod} UniSender method was called for following list ids {separatedListIds}");
      var url = $"/{ExcludeMethod}?format=json&api_key={_key}&contact={contact}&contact_type={type}&list_ids={separatedListIds}";
      return await _client.GetAsync<ExcludeResponseMessage>(url);
    }

    public async Task<ContactResponseMessage>  GetContactAsync(string type, string contact)
    {
      _logger.LogDebug($"{GetContactMethod} UniSender method was called for {type}: {contact}");
      var url = $"/{GetContactMethod}?format=json&api_key={_key}&{type}={contact}";
      return await _client.GetAsync<ContactResponseMessage>(url);
    }

    public async Task<IsContactInListsResponse> IsContactInListsAsync(string email, List<long> listIds, string condition)
    {
      string commaSeperatedListIds = string.Join(",", listIds);
      _logger.LogDebug($"{IsContactInListsMethod} UniSender method was called for to check in {commaSeperatedListIds}");
      var url = $"/{IsContactInListsMethod}?format=json&api_key={_key}&email={email}&list_ids={commaSeperatedListIds}&condition={condition}";
      
      return await _client.GetAsync<IsContactInListsResponse>(url);
    }

    public async Task<ImportContactsResponse> ImportContactsAsync(List<Contact> contacts)
    {
      string url = $"/{ImportContactsMethod}";
      _logger.LogDebug($"{ImportContactsMethod} UniSender method was called for {contacts.Count} emails");

      var parameters = new RequestParameters<ImportContactsResponse>() {OnBeforeRequestSend = FillRequestParameters};
      
      void FillRequestParameters(IRestRequest request)
      {
        request.AddHeader("content-type", "application/x-www-form-urlencoded");
        request.AddParameter("format", "json");
        request.AddParameter("api_key", _key);

        request.AddParameter("field_names[0]", "email");
        request.AddParameter("field_names[1]", "Name");
        request.AddParameter("field_names[2]", "email_list_ids");

        for (var i = 0; i < contacts.Count; i++)
        {
          request.AddParameter($"data[{i}][0]", contacts[i].Email);
          request.AddParameter($"data[{i}][1]", contacts[i].Name);
          request.AddParameter($"data[{i}][2]", contacts[i].ListId);
        }
      }

      var response = await _client.PostAsync<ImportContactsResponse>(url, null, parameters);
      return response;
    }
  }
}
