﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace XCritical.Crm.MailList.Logic.Services.SyncService
{
  public interface IMailListSyncService
  {
    Task Synchronize();
  }
}
