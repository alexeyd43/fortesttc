﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XCritical.Crm.MailList.Logic.Services.SyncScheduleService
{
  public class SyncSchedule
  {
    public TimeSpan SyncAt { get; set; }
    public string Days { get; set; }
    public bool ByWeekdays { get; set; }
    public bool Enabled { get; set; }
  }
}
