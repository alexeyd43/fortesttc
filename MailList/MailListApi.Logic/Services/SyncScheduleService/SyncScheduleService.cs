﻿using JobScheduler.Client;
using JobScheduler.Client.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace XCritical.Crm.MailList.Logic.Services.SyncScheduleService
{
  public class SyncScheduleService : ISyncScheduleService
  {
    //private SyncService _syncService; TODO:
    private IJobSchedulerClient _schedulerClient;
    private SchedulerConfiguration _configuration;

    public SyncScheduleService(IJobSchedulerClient schedulerClient, SchedulerConfiguration configuration)
    {
      _schedulerClient = schedulerClient;
      _configuration = configuration;
    }

    public Task<SyncSchedule> GetSyncScheduleAsync()
    {
      //return _scheduleRepo.GetSyncScheduleAsync(); TODO:
      throw new NotImplementedException();
    }
    public async Task UpsertSyncScheduleAsync(SyncSchedule syncSchedule)
    {
      if (syncSchedule == null)
        throw new ArgumentNullException(nameof(syncSchedule));

      await _schedulerClient.DeleteJobAsync(_configuration.SyncScheduleId);

      if (!syncSchedule.Enabled)
        return;

      await _schedulerClient.CreateJobAsync(new Job
      {
        Id = _configuration.SyncScheduleId,
        FireTime = null,
        JobType = JobType.MailGroupsSynchronizationTrigger,
        Schedule = GetScheduleExpression(syncSchedule)
      });

      //await _syncService.UpsertSyncScheduleAsync(syncSchedule); TODO:
    }

    private string GetScheduleExpression(SyncSchedule schedule)
    {
      if (string.IsNullOrEmpty(schedule.Days))
        throw new Exception("Schedule expression is null or empty.");

      if (schedule.ByWeekdays)
      {
        var days = schedule.Days.Split(',').Select(x =>
        {
          if (!int.TryParse(x, out int day) || day < 1 || day > 7)
            throw new Exception("Wrong days format.");

          return day;
        }).OrderBy(x => x);

        if (days.Count() != days.Distinct().Count())
          throw new Exception("Wrong format: Repeated days.");

        var daysExpression = days.Count() == 7 ? "*" : string.Join(",", days);

        return $"0 {schedule.SyncAt.Minutes} {schedule.SyncAt.Hours} ? * {daysExpression} *";
      }

      if (!int.TryParse(schedule.Days, out int syncDays) || syncDays < 1 || syncDays > 31)
        throw new Exception("Wrong day format.");

      return $"0 {schedule.SyncAt.Minutes} {schedule.SyncAt.Hours} 1/{syncDays} * ? *";
    }
  }
}
