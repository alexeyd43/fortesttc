﻿using System.Threading.Tasks;

namespace XCritical.Crm.MailList.Logic.Services.SyncScheduleService
{
  public interface ISyncScheduleService
  {
    Task<SyncSchedule> GetSyncScheduleAsync();
    Task UpsertSyncScheduleAsync(SyncSchedule syncSchedule);
  }
}
